//
//  User.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/15/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import UIKit

struct User {
    var name: String?
    var uuid: String?
    var phone: String?
    var type: String?
}

extension User {
    
}
