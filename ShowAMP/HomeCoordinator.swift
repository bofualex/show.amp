//
//  HomeCoordinator.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

final class HomeCoordinator {
    
    fileprivate var homeViewController: HomeViewController?
    
    //MARK: - memory management
    init() {

    }
    
    //MARK: - public
    func initHomeView() -> UIViewController {
        homeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        SVProgressHUD.show()
        
        DatabaseManager.getAds(with: { [weak self] (adds) in
            self?.homeViewController?.adds = adds!
            self?.homeViewController?.addTableView.reloadData()
            SVProgressHUD.dismiss()
        })
        
        return homeViewController!
    }
    
    //MARK: - private
}
