//
//  CustomTextField.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    override init(frame:CGRect) {
        super.init(frame:frame)
        
        defaultInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        defaultInit()
    }
    
    override func becomeFirstResponder() -> Bool {
        textColor = Constants.brandingColorSelected

        return super.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        textColor = Constants.brandingColorUnselected

        return super.resignFirstResponder()
    }
    
    func defaultInit() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 5))
        leftView = paddingView;
        leftViewMode = .always;
        layer.borderWidth = 0.5
        layer.borderColor = Constants.brandingColorUnselected.cgColor
        backgroundColor = UIColor(red:0.93, green:0.94, blue:0.95, alpha:1.0)
        textColor = Constants.brandingColorUnselected
    }
}
