//
//  ViewHelper.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/16/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func blink(_ value: Bool) {
        if value {
            UIView.animate(withDuration: 1,
                           delay: 0.0,
                           options: [.curveEaseInOut, .autoreverse, .repeat],
                           animations: { [weak self] in self?.alpha = 0.1; self?.backgroundColor = UIColor(red:0.98, green:0.37, blue:0.35, alpha:1.0)},
                           completion: { [weak self] _ in self?.alpha = 1; self?.backgroundColor = .clear})
        } else {
            UIView.animate(withDuration: 1,
                           delay: 0.0,
                           options: [.curveEaseInOut, .beginFromCurrentState],
                           animations: {[weak self] in self?.alpha = 1.0; self?.backgroundColor = .clear},
                           completion: nil)
        }
    }
    
    func addShadow() {
        self.clipsToBounds = true
        let radius: CGFloat = self.frame.width / 2.0
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 2.1 * radius, height: self.frame.height))
        
        self.layer.cornerRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 5.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = shadowPath.cgPath
    }
}

extension UIViewController {
    class func fromNib<T : UIViewController>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
