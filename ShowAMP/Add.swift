//
//  Add.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Add {
    var title: String?
    var id: Int?
    var userId: Int?
    var description: String?
    var category: String?
    var userImage: String?
    var userName: String?
    var image: UIImage?
}

extension Add {
    static func parseAdds(for json: NSArray) -> [Add] {
        var adds = [Add]()
        
        for subJSON in json {
            let subJSON = JSON(subJSON)
            
            var add = Add()
            
            if let category = subJSON["category"].string {
                add.category = category
            }
            
            if let description = subJSON["description"].string {
                add.description = description
            }
            
            if let title = subJSON["title"].string {
                add.title = title
            }
            
            if let userId = subJSON["userId"].int {
                add.userId = userId
            }
            
            if let userImage = subJSON["userLogoImage"].string {
                add.userImage = userImage
            }
            
            if let userName = subJSON["userName"].string {
                add.userName = userName
            }
            
            adds.append(add)
        }
        
        return adds
    }
}
