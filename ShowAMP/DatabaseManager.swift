//
//  DatabaseManager.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DatabaseManager: Firebase {
    
    static func getAds(with completion: addsClosure) {
        databaseRef.child("Adds/").observeSingleEvent(of: .value, with: { (snapshot) in
            if let json = snapshot.value as? NSMutableArray {
                json.removeObject(at: 0)
                completion?(Add.parseAdds(for: json))
            } else {
                completion?([])
            }
        })
    }
    
    static func findUsersAndAdds<T: Equatable>(text: String, completion: searchResultsClosure<T>) {
        databaseRef.child("Users").queryOrdered(byChild: "name").queryStarting(atValue: text).queryEnding(atValue: text+"\u{f8ff}").observe(.value, with: { snapshot in
            var user = User()
            var users = [User]()
            
            for u in snapshot.children {
                user.name = (u as! NSDictionary)["name"] as? String
                users.append(user)
            }
            
        })
    }
}
