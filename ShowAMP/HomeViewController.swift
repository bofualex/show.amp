//
//  HomeViewController.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit
import AlamofireImage
import FLKAutoLayout

class HomeViewController: UIViewController {
    
    @IBOutlet weak var addTableView: UITableView!
    @IBOutlet weak var searchResultsTableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchViewLeadingConstraint: NSLayoutConstraint!
    
    var adds = [Add]()
    var previousSelectedIndex: IndexPath?
    
    fileprivate let placeholderImage = UIImage(size: CGSize(width: 60, height: 60))
    fileprivate var isShowingAccesory: Bool = false
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - private
    fileprivate func setupUI() {
        searchViewLeadingConstraint.constant = view.frame.width
        addTableView.rowHeight = UITableViewAutomaticDimension
        addTableView.estimatedRowHeight = 280
        searchResultsTableView.rowHeight = UITableViewAutomaticDimension
        searchResultsTableView.estimatedRowHeight = 80
        searchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    //MARK: - actions
    @IBAction func onSearch() {
        if searchViewLeadingConstraint.constant == 81 {
            searchViewLeadingConstraint.constant = view.frame.width
        } else {
            searchViewLeadingConstraint.constant = 81
        }
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        })
    }
    
    @IBAction func onAccesory(sender: UIButton) {
        let buttonOriginInTableView = sender.convert(CGPoint.zero, to: addTableView)
        let indexPath = addTableView.indexPathForRow(at: buttonOriginInTableView)!
        
        if addTableView.indexPathsForVisibleRows!.contains(indexPath) {
            let cell = addTableView.cellForRow(at: indexPath) as! AddTableViewCell
            
            switch sender.tag {
            case 0:
                cell.showAccesory(for: .phone)
            case 1:
                cell.showAccesory(for: .message)
            default: break
            }
            
            if cell.isShowingAccesory {
                sender.tintColor = Constants.brandingColorSelected
                previousSelectedIndex = indexPath
            } else {
                previousSelectedIndex = nil
            }
        }
        
        addTableView.reloadData()
    }
}

extension HomeViewController: UITextFieldDelegate {
    @objc  func textFieldDidChange(sender: UITextField) {
        if sender.text!.characters.count < 2 {
            view.bringSubview(toFront: addTableView)
        } else {
            view.bringSubview(toFront: searchResultsTableView)
        }
    }
}

extension HomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == addTableView{
            return adds.count
        } else {
            return 8
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        if tableView == addTableView {
            cell = tableView.dequeueReusableCell(withIdentifier: "AddCell", for: indexPath) as! AddTableViewCell
            (cell as! AddTableViewCell).setupCell(for: adds[indexPath.row])
            (cell as! AddTableViewCell).userImageView.image = nil
            
            if previousSelectedIndex == indexPath {
                (cell as! AddTableViewCell).isShowingAccesory = true
            } else {
                for subview in (cell as! AddTableViewCell).containerView.subviews {
                    subview.removeFromSuperview()
                }
            }
            
            if adds[indexPath.row].image != nil {
                (cell as! AddTableViewCell).userImageView.image = adds[indexPath.row].image
            } else if let image = adds[indexPath.row].userImage {
                (cell as! AddTableViewCell).userImageView?.af_setImage(withURL: URL(string: image)!, placeholderImage: placeholderImage, completion: { [weak self] (image) in
                    self?.adds[indexPath.row].image = UIImage(data: image.data!)
                })
            }
        } else {
            if indexPath.row < 2 {
                cell = tableView.dequeueReusableCell(withIdentifier: "SmallAddCell", for: indexPath) as! SmallAddTableViewCell
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "PersonCell", for: indexPath) as! PersonTableViewCell
            }
        }
        
        return cell
    }
}
