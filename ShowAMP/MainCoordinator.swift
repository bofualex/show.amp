//
//  MainCoordinator.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import UIKit

final class MainCoordinator {
    
    fileprivate let homeCoordinator = HomeCoordinator()
    fileprivate let notificationCoordinator = NotificationsCoordinator()
    fileprivate let messagesCoordinator = MessagesCoordinator()
    fileprivate let profileCoordinator = ProfileCoordinator()

    fileprivate var tabBarController: TabBarController?
    
    func initTabBar() -> UITabBarController {
        tabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarController
        tabBarController?.viewControllers = tabBarControllers()
        
        return tabBarController!
    }
    
    func tabBarControllers() -> [UIViewController] {
        return [homeCoordinator.initHomeView(), notificationCoordinator.initNotificationsView(), messagesCoordinator.initMessagesView(),
        profileCoordinator.initProfileView()]
    }
}

