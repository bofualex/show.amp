//
//  AddTableViewCell.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class AddTableViewCell: UITableViewCell {

    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var reportButton: UIButton!
    
    var buttonsArray: [UIButton] {
        return [callButton, messageButton, favouriteButton, shareButton, reportButton]
    }
    var phoneView: PhoneView?
    var messageView: MessageView?
    var isShowingAccesory: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(for add: Add) {
        titleLabel.text = add.title
        descriptionLabel.text = add.description
        userNameLabel.text = add.userName
    }

    func showAccesory(for cellType: CellAccesoryType) {
        isShowingAccesory = !isShowingAccesory
        
        for button in buttonsArray {
            button.tintColor = Constants.brandingColorUnselected
        }
        
        for subview in containerView.subviews {
            subview.removeFromSuperview()
        }
        
        phoneView = nil
        messageView = nil
        
        if isShowingAccesory {
            switch cellType {
            case .message:
                messageView = MessageView.getMessageView()
                containerView.addSubview(messageView!)
                messageView?.align(toView: containerView)
            case .phone:
                phoneView = PhoneView.getPhoneView()
                containerView.addSubview(phoneView!)
                phoneView?.align(toView: containerView)
            }
        }
    }
}
