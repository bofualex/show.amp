//
//  Firebase.swift
//  wedding
//
//  Created by Bofu Alex on 18/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import FirebaseStorage

protocol Firebase {
    static var databaseRef: FIRDatabaseReference { get }
    static var storageRef: FIRStorageReference { get }
}

extension Firebase {
    static var databaseRef: FIRDatabaseReference {
        return FIRDatabase.database().reference()
    }
    static var storageRef: FIRStorageReference {
        return FIRStorage.storage().reference()
    }
}
