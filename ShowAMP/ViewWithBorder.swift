//
//  ViewWithBorder.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/15/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class ViewWithBorder: UIView {

    override init(frame:CGRect) {
        super.init(frame:frame)
        
        defaultInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        defaultInit()
    }
    
    func defaultInit() {
        layer.borderWidth = 0.5
        layer.borderColor = UIColor(red:0.56, green:0.63, blue:0.64, alpha:0.75).cgColor
        layer.cornerRadius = 3
    }
}
