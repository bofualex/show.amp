//
//  Notifications.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import UIKit

final class NotificationsCoordinator {
    fileprivate var notificationsView: NotificationsViewController?
    
    //MARK: - memory management
    init() {
        
    }
    
    //MARK: - public
    func initNotificationsView() -> UIViewController {
        notificationsView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController
        
        return notificationsView!
    }
    
    //MARK: - private
}
