//
//  Constants.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import UIKit

typealias addsClosure = (([Add]?) -> ())?
typealias searchResultsClosure<T: Equatable> = (([T]?) -> ())?

enum CellAccesoryType: Int {
    case message = 0
    case phone = 1
}

extension CellAccesoryType: RawRepresentable {
    typealias RawValue = UIView
    
    init?(rawValue: RawValue) {
        switch rawValue {
        case is PhoneView: self = .phone
        case is MessageView: self = .message
        default: return nil
        }
    }
    
    var rawValue: RawValue {
        switch self {
        case .message: return MessageView()
        case .phone: return PhoneView()
        }
    }
}

struct Constants {
    static let brandingColorSelected = UIColor(red:0.20, green:0.60, blue:0.86, alpha:1.0)
    static let brandingColorUnselected = UIColor(red:0.56, green:0.63, blue:0.64, alpha:1.0)
}
