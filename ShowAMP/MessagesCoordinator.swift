//
//  MessagesCoordinator.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import UIKit

final class MessagesCoordinator {
    fileprivate var messagesView: MessagesViewController?
    
    //MARK: - memory management
    init() {
        
    }
    
    //MARK: - public
    func initMessagesView() -> UIViewController {
        messagesView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessagesViewController") as? MessagesViewController
        
        return messagesView!
    }
    
    //MARK: - private
}
