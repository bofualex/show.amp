//
//  ProfileCoordinator.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import UIKit

final class ProfileCoordinator {
    fileprivate var profileView: ProfileViewController?
    
    //MARK: - memory management
    init() {
        
    }
    
    //MARK: - public
    func initProfileView() -> UIViewController {
        profileView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
        
        return profileView!
    }
    
    //MARK: - private
}
