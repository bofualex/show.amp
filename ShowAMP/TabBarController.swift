//
//  TabBarController.swift
//  ShowAMP
//
//  Created by Alex Bofu on 5/14/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - private
    fileprivate func setupUI() {
        tabBar.tintColor = Constants.brandingColorSelected
        tabBar.backgroundColor = .white
        
        if #available(iOS 10.0, *) {
            tabBar.unselectedItemTintColor = Constants.brandingColorUnselected
        } else {
            
        }
    }
}
